#!/usr/bin/env xonsh

$XONSH_SHOW_TRACEBACK = True

from pathlib import Path

BASHRC = $HOME+"/.bashrc"
ALIAS= $HOME+"/.scone/alias"

aliasCommand = "# Created by 'SCONE add_alias' on "+$(date)+"\
alias scone=\"docker run -it --rm \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v \""+$HOME+"/.docker:/root/.docker\" \
    -v \""+$HOME+"/.cas:/root/.cas\" \
    -v \""+$HOME+"/.scone:/root/.scone\" \
    -v \""+$PWD+":/root\" \
    -w /root \
    registry.scontain.com:5050/community/cli scone\"\n"

#Create directory if not existing to make the script idempotent
Path($HOME+"/.scone").mkdir(parents=True, exist_ok=True)
Path($HOME+"/.cas").mkdir(parents=True, exist_ok=True)

#Add alias to .bashrc file
$(grep "alias scone" @(BASHRC)) || $(echo @(aliasCommand) >> @(BASHRC))

#Check if we have permission to write in $HOME/.scone/state.env
!(touch $HOME/.scone/state.env) || print("Warning : Unable to write in "+$HOME+"/.scone/")

#Check if alias is existing
$(grep "alias scone" @(BASHRC)) && print("Alias successfully found !")